import pandas as pd

GTFS_folder = 'GTFS/'

routes_df = pd.read_csv(GTFS_folder + 'routes.txt')
trips_df = pd.read_csv(GTFS_folder + 'trips.txt')
stops_df = pd.read_csv(GTFS_folder + 'stops.txt')
stop_times_df = pd.read_csv(GTFS_folder + 'stop_times.txt')


def print_routes():
    print(routes_df)


def print_stops():
    print(stops_df)


def print_trips():
    print(trips_df)


def print_stop_times():
    print(stop_times_df)


def find_route_by_route_id(route_id):
    return routes_df[routes_df.route_id == route_id].values()


def find_stops_by_stop_id(stop_id):
    return stops_df[stops_df.stop_id == stop_id].values()


def get_routes_by_agency(agency_id):
    return routes_df[routes_df.agency_id == agency_id].values()


def get_trips_of_a_route(route_id):
    return trips_df[trips_df.route_id == route_id].values()


if __name__ == '__main__':
    pass
